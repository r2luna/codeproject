angular.module('app.factories')
    .factory('ClientTypeaheadFactory', function() {
        return function($scope, Client, clientDefault) {

            // Cliente formatado
            $scope.clientFormatedName = clientDefault ? clientDefault : '';

            // É executado quando o cliente é selecionado
            // pegamos o nome selecionado para formatar a saída
            $scope.onSelectClient = function(model) {
                $scope.project.client_id = model.id;
            };

            // Formata o resultado do Typeahead
            // do campo do cliente, ao invés
            // de retornar o id retorna o nome
            $scope.onBlurFormatName = function(model) {
                if( typeof model == 'object' ) return model.name;
            };

            // Consulta os clients sempre que algo é digitado no campo
            // Assim podemos garantir de traremos os clientes existentes na base
            // mesmo que alguém esteja cadastrando/editando um cliente nesse momento.
            $scope.getClients = function(name) {
                return Client.cs({
                    search: name,
                    searchFields: 'name:like'
                }).$promise;
            };
        };
    });
