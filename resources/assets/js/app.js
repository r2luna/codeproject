// --
// Início da aplicação : instancia a aplicação do angularJs,
// informando suas dependências []
var app = angular.module('app', [
    'ngRoute',
    'angular-oauth2',
    'app.controllers',
    'app.filters',
    'app.services',
    'app.factories',
    'ui.bootstrap',
    'ui.bootstrap.tpls'
]);

// --
// Controllers
// Carrega o app.controllers que é o namespace
// de todos os controladores, informando também suas dependências []
angular.module('app.controllers', ['angular-oauth2', 'ngMessages']);

// --
// Filters
angular.module('app.filters', []);

// --
// Services
// Carrega o serviço que iremos utilizar para comunicação com a api
angular.module('app.services', ['ngResource']);

// --
// Factories
angular.module('app.factories', []);

// --
// appConfigProvider: provider que possuirá todas as configurações
// globais da aplicação
app.provider('appConfig', function() {
    var config = {
        baseUrl: 'http://codeproject.dev/api',
        project: {
            status: [{
                value: 1,
                label: 'Not started'
            }, {
                value: 2,
                label: 'Started'
            }, {
                value: 3,
                label: 'Completed'
            }]
        },
        utils: {

            // --
            // transformResponse global
            transformResponse: function(data, headers) {

                // Carrega todas as informações de header
                var headersGetter = headers();

                // --
                // Verifica se o retorno do ajax é to tipo json
                // * todas as requisições de views também são
                // * feitas via json, e se não fizermos essa tratativa
                // * vamos tratar a view.
                if (
                    headersGetter['content-type'] == 'application/json' ||
                    headersGetter['content-type'] == 'text/json'
                ) {
                    // --
                    // Converte a informação para JSON, por padrão o
                    // tipo de variável é string
                    var dataJson = JSON.parse(data);

                    // --
                    // Confirma se o json tem de fato a propriedade 'data'
                    if (dataJson.hasOwnProperty('data')) {
                        dataJson = dataJson.data;
                    }

                    // --
                    // Retorna o Json ( tratado ou não )
                    return dataJson;

                }

                // Se não for, retorna o conteúdo recebido
                else {

                    return data;

                }
            }

            // --
            // Tive que adicionar um interceptor para quando der erro
            // de acesso negado pq o listener do OAuth não tava rolando
            // legal, então quando dá erro de acesso negado ele dispara
            // o oauth:error
            // --
            // Pelo visto voltou a funcionar o listener
            // --
            // lnOAuthInterceptor: function($q, $rootScope, OAuthToken) {
            //     return {
            //         responseError: function(rejection) {
            //             if (401 === rejection.status && rejection.data && 'access_denied' === rejection.data.error) {
            //                 $rootScope.$emit('oauth:error', rejection);
            //             }
            //             return $q.reject(rejection);
            //         }
            //     };
            // }
        }
    };

    return {
        config: config,
        $get: function() {
            return config;
        }
    };

});

// --
// Configuração da aplicação: rotas e configuração do oAuth2
app.config([
    '$routeProvider', '$httpProvider', 'OAuthProvider', 'OAuthTokenProvider', 'appConfigProvider',
    function($routeProvider, $httpProvider, OAuthProvider, OAuthTokenProvider, appConfigProvider) {
        // ==
        // Esta opção serve para transformar a resposta
        // que vem do servidor, quando utilizamos os transformers
        // no Laravel\Fractal por padrão ele envia as informações
        // dentro de um objeto chamado 'data', e o angular se perde
        // com isso, por isso por isso é preciso transformar o resultado.
        // --
        // Mas, eu já tinha modificado o retorno do Laravel, criando uma
        // classe intermediária de Serializer: DataArraySerializer.php
        // que remove o data na transformação dos dados, então
        // não é preciso fazer nada no angular.
        // --
        // Apenas lembrando que precisa ter a chamada do $httpProvider no
        // início da function do app.config
        // --
        // Quando utilizamos algum transformResponse no serviço diretamente
        // ele sobrescreve o transformResponse global abaixo. Uma solução
        // para isso é migrar o transformResponse para o provider (pode ser
        // no appConfigProvider que já existe, então vou criar uma objeto
        // chamado utils que vai armazenar funções utéis para todo o sistema )
        // appConfigProvider.config.utils.transformResponse()
        // --
        $httpProvider.defaults.transformResponse = appConfigProvider.config.utils.transformResponse;

        // ==
        // Configurando as rotas
        $routeProvider
            .when('/login', {
                templateUrl: 'build/views/login.html',
                controller: 'LoginController'
            })
            .when('/home', {
                templateUrl: 'build/views/home.html',
                controller: 'HomeController'
            })
            .when('/clients', {
                templateUrl: 'build/views/client/list.html',
                controller: 'ClientListController'
            })
            .when('/clients/new', {
                templateUrl: 'build/views/client/form.html',
                controller: 'ClientNewController'
            })
            .when('/clients/:id/edit', {
                templateUrl: 'build/views/client/form.html',
                controller: 'ClientEditController'
            })
            .when('/clients/:id/delete', {
                templateUrl: 'build/views/client/form.html',
                controller: 'ClientDeleteController'
            })
            .when('/projects', {
                templateUrl: 'build/views/project/list.html',
                controller: 'ProjectListController'
            })
            .when('/project/new', {
                templateUrl: 'build/views/project/form.html',
                controller: 'ProjectNewController'
            })
            .when('/project/:id/edit', {
                templateUrl: 'build/views/project/form.html',
                controller: 'ProjectEditController'
            })
            .when('/project/:id/delete', {
                templateUrl: 'build/views/project/form.html',
                controller: 'ProjectDeleteController'
            })
            .when('/project/:id/notes', {
                templateUrl: 'build/views/projectNote/list.html',
                controller: 'ProjectNoteListController'
            })
            .when('/project/:id/notes/new', {
                templateUrl: 'build/views/projectNote/form.html',
                controller: 'ProjectNoteNewController'
            })
            .when('/project/:id/notes/:idNote/edit', {
                templateUrl: 'build/views/projectNote/form.html',
                controller: 'ProjectNoteEditController'
            })
            .when('/project/:id/notes/:idNote/delete', {
                templateUrl: 'build/views/projectNote/form.html',
                controller: 'ProjectNoteDeleteController'
            });

        // ==
        // Configuração do oAuth2
        OAuthProvider.configure({
            baseUrl: appConfigProvider.config.baseUrl,
            clientId: 'appid1',
            clientSecret: 'secret',
            grantPath: 'oauth/access_token'
        });

        // ==
        // Por padrão, o oAuth2 trabalha com https
        // precisamos configurar manualmente para trabalhar com http
        OAuthTokenProvider.configure({
            name: 'token',
            options: {
                secure: false
            }
        });

        // ==
        // Interceptors
        // $httpProvider.interceptors.push( appConfigProvider.config.utils.lnOAuthInterceptor );
    }
]);

// ==
// Configuração do oAuth2
// https://github.com/oauthjs/angular-oauth2
app.run(['$rootScope', '$window', 'OAuth', function($rootScope, $window, OAuth) {
    $rootScope.$on('oauth:error', function(event, rejection) {

        // Ignora o erro 'invalid_grant'
        if ('invalid_grant' === rejection.data.error) {
            return;
        }

        // Atualiza o token caso 'invalid_token' ocorra
        if ('invalid_token' === rejection.data.error) {
            return OAuth.getRefreshToken();
        }

        // Muda o texto caso
        if ('invalid_request' === rejection.data.error) {
            rejection.data.error = 'Access denied!';
        }

        // Redireciona para '/login' com a razão do erro 'error_reason'
        $window.location.href = '/#/login?error_reason=' + rejection.data.error;

    });
}]);
