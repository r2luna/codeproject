angular.module('app.controllers')
    .controller('ClientDeleteController', 
        ['$scope', '$routeParams', 'Client', '$location', function ($scope, $routeParams, Client, $location) {

        $scope.client = Client.get({ id: $routeParams.id });
        $scope.forDeletion = true;

        $scope.error = {
            error: false,
            message: ''
        };

        $scope.save = function () {

            $scope.client.$delete().then(function (data) {
                $scope.error.error = data.error;
                if( data.error ) {
                    $scope.error.message = data.message;
                    $scope.client = Client.get({ id: $routeParams.id });
                } else {
                    $location.path('/clients');
                }
            });

        };
        
    }]);