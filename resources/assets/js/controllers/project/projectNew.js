angular.module('app.controllers')
    .controller('ProjectNewController', ['$scope', 'Project', 'Client', '$location', '$cookies', 'appConfig', '$injector', 'ClientTypeaheadFactory',
        function($scope, Project, Client, $location, $cookies, appConfig, $injector, ClientTypeaheadFactory) {

            // Carrega o modelo do projeto
            $scope.project = new Project();

            // Carrega a lista de
            $scope.status = appConfig.project.status;
            $scope.error = {
                error: false,
                message: ''
            };

            // --
            // Método para salvar o projeto
            $scope.save = function() {
                if ($scope.form.$valid) {

                    // Define o dono do projeto
                    $scope.project.owner_id = $cookies.getObject('user').id;

                    // Salva o projeto
                    $scope.project.$save().then(function(data) {
                        $scope.error.error = data.error;
                        if (data.error) {
                            $scope.error.message = data.message;
                        } else {
                            $location.path('/projects');
                        }
                    });

                }
            };

            // --
            // Insere os métodos de controle do campo de cliente typeahead
            $injector.invoke(ClientTypeaheadFactory, this, {
                $scope: $scope,
                Client: Client,
                clientDefault: null
            });

            // --
            // Para ativar o DatePicker
            $scope.due_date = {
                status: {
                    opened: false
                }
            };

            $scope.openDatePicker = function($event) {
                $scope.due_date.status.opened = true;
            };

        }
    ]);
