angular.module('app.controllers')
    .controller('ProjectEditController', ['$scope', '$routeParams', 'Project', 'Client', '$location', 'appConfig', '$injector', 'ClientTypeaheadFactory',
        function($scope, $routeParams, Project, Client, $location, appConfig, $injector, ClientTypeaheadFactory) {

            // Faz a consulta do projeto
            Project.get({
                id: $routeParams.id

                // No sucesso da consulta executa os demais scripts
            }, function(data) {

                // Seta o projeto no $scope
                $scope.project = data;

                // Insere os métodos de controle do campo de cliente typeahead
                $injector.invoke(ClientTypeaheadFactory, this, {
                    $scope: $scope,
                    Client: Client,
                    clientDefault: data.client
                });

            });

            // Lista de status disponível
            $scope.status = appConfig.project.status;

            // Variável de controle de erro
            $scope.error = {
                error: false,
                message: ''
            };

            // Método para salvar o registro
            $scope.save = function() {
                if ($scope.form.$valid) {

                    $scope.project.owner_id = $scope.project.owner.id;

                    Project.update({
                        id: $scope.project.project_id
                    }, $scope.project, function(data) {
                        $scope.error.error = data.error;
                        if (data.error) {
                            $scope.error.message = data.message;
                            $scope.project = Project.get({
                                id: $routeParams.id
                            });
                        } else {
                            $location.path('/projects');
                        }
                    });

                }
            };

            // --
            // Para ativar o DatePicker
            $scope.due_date = {
                status: {
                    opened: false
                }
            };

            $scope.openDatePicker = function($event) {
                $scope.due_date.status.opened = true;
            };

        }
    ]);
