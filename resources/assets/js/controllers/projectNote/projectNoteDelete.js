angular.module('app.controllers')
    .controller('ProjectNoteDeleteController', ['$scope', '$routeParams', 'ProjectNote', '$location', function($scope, $routeParams, ProjectNote, $location) {

        $scope.note = ProjectNote.get({
            id: $routeParams.id,
            idNote: $routeParams.idNote
        });
        $scope.forDeletion = true;
        $scope.project_id = $routeParams.id;

        $scope.error = {
            error: false,
            message: ''
        };

        $scope.save = function() {

            $scope.note.$delete({
                id: $routeParams.id,
                idNote: $routeParams.idNote
            }).then(function(data) {
                $scope.error.error = data.error;
                if (data.error) {
                    $scope.error.message = data.message;
                    $scope.note = ProjectNote.get({
                        id: $routeParams.id,
                        idNote: $routeParams.idNote
                    });
                } else {
                    $location.path('/project/' + $routeParams.id + '/notes');
                }
            });

        };

    }]);
