angular.module('app.controllers')
    .controller('LoginController', ['$scope', '$location', 'OAuth', 'User', '$cookies', '$routeParams',
        function($scope, $location, OAuth, User, $cookies, $routeParams) {
            $scope.user = {
                username: '',
                password: ''
            };

            $scope.error = {
                error: $routeParams.error_reason !== undefined,
                message: $routeParams.error_reason !== undefined ? $routeParams.error_reason : ''
            };

            $scope.login = function() {
                if ($scope.form.$valid) {
                    OAuth.getAccessToken($scope.user)
                        .then(function() {
                            User.authenticated({}, {}, function(data){
                                $cookies.putObject('user', data);
                                $location.path('home');
                            });
                        }, function(data) {
                            $scope.error.error = true;
                            $scope.error.message = data.data.error_description;
                        });
                }
            };
        }
    ]);
