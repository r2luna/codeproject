angular.module('app.controllers')
    .controller('ClientEditController', ['$scope', '$routeParams', 'Client', '$location',
        function($scope, $routeParams, Client, $location) {

            $scope.client = Client.get({
                id: $routeParams.id
            });

            $scope.error = {
                error: false,
                message: ''
            };

            $scope.save = function() {
                if ($scope.form.$valid) {

                    Client.update({
                        id: $scope.client.id
                    }, $scope.client, function(data) {
                        $scope.error.error = data.error;
                        if (data.error) {
                            $scope.error.message = data.message;
                            $scope.client = Client.get({
                                id: $routeParams.id
                            });
                        } else {
                            $location.path('/clients');
                        }
                    });

                }
            };

        }
    ]);
