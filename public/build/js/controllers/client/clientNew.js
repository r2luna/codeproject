angular.module('app.controllers')
    .controller('ClientNewController', ['$scope', 'Client', '$location',
        function($scope, Client, $location) {

            $scope.client = new Client();
            $scope.error = {
                error: false,
                message: ''
            };

            $scope.save = function() {
                if ($scope.form.$valid) {

                    $scope.client.$save().then(function(data) {
                        $scope.error.error = data.error;
                        if (data.error) {
                            $scope.error.message = data.message;
                        } else {
                            $location.path('/clients');
                        }
                    });

                }
            };

        }
    ]);
