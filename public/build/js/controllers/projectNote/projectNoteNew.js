angular.module('app.controllers')
    .controller('ProjectNoteNewController', ['$scope', 'ProjectNote', '$location', '$routeParams',
        function($scope, ProjectNote, $location, $routeParams) {

            $scope.note = new ProjectNote();
            $scope.project_id = $routeParams.id;

            $scope.error = {
                error: false,
                message: ''
            };

            $scope.save = function() {
                if ($scope.form.$valid) {
                    $scope.note.project_id = $routeParams.id;
                    $scope.note.$save({id: $routeParams.id}).then(function(data) {
                        $scope.error.error = data.error;
                        if (data.error) {
                            $scope.error.message = data.message;
                        } else {
                            $location.path('/project/' + $routeParams.id + '/notes');
                        }
                    });

                }
            };

        }
    ]);
