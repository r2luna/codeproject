angular.module('app.controllers')
    .controller('ProjectNoteEditController', ['$scope', '$routeParams', 'ProjectNote', '$location',
        function($scope, $routeParams, ProjectNote, $location) {

            $scope.note = ProjectNote.get({
                id: $routeParams.id,
                idNote: $routeParams.idNote
            });

            $scope.project_id = $routeParams.id;

            $scope.error = {
                error: false,
                message: ''
            };

            $scope.save = function() {
                if ($scope.form.$valid) {

                    delete $scope.note.id;

                    ProjectNote.update({
                        id: $routeParams.id,
                        idNote: $routeParams.idNote
                    }, $scope.note, function(data) {
                        $scope.error.error = data.error;
                        if (data.error) {
                            $scope.error.message = data.message;
                            $scope.note = ProjectNote.get({
                                id: $routeParams.id,
                                idNote: $routeParams.idNote
                            });
                        } else {
                            $location.path('/project/' + $routeParams.id + '/notes');
                        }
                    });

                }
            };

        }
    ]);
