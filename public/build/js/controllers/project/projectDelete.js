angular.module('app.controllers')
    .controller('ProjectDeleteController', ['$scope', '$routeParams', 'Project', 'Client', '$location', 'appConfig', '$injector', 'ClientTypeaheadFactory',
        function($scope, $routeParams, Project, Client, $location, appConfig, $injector, ClientTypeaheadFactory) {

            Project.get({
                id: $routeParams.id
            }, function(data){
                // Seta o projeto no $scope
                $scope.project = data;

                // Insere os métodos de controle do campo de cliente typeahead
                $injector.invoke(ClientTypeaheadFactory, this, {
                    $scope: $scope,
                    Client: Client,
                    clientDefault: data.client
                });

            });

            $scope.forDeletion = true;
            $scope.status = appConfig.project.status;

            $scope.error = {
                error: false,
                message: ''
            };

            $scope.save = function() {
                $scope.project.owner_id = $scope.project.owner.id;

                $scope.project.$delete().then(function(data) {
                    $scope.error.error = data.error;
                    if (data.error) {
                        $scope.error.message = data.message;
                        $scope.project = Project.get({
                            id: $routeParams.id
                        });
                    } else {
                        $location.path('/projects');
                    }
                });

            };

        }
    ]);
