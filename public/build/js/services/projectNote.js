angular.module('app.services')
    .service('ProjectNote', ['$resource', 'appConfig', '$routeParams',
        function($resource, appConfig, $routeParams) {
            return $resource(appConfig.baseUrl + '/project/:id/note/:idNote', {
                id: '@id',
                idNote: '@idNote'
            }, {
                update: {
                    method: 'PUT'
                }
            });
        }
    ]);
