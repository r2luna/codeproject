angular.module('app.services')
    .service('Project', ['$resource', 'appConfig', function($resource, appConfig) {
        return $resource(appConfig.baseUrl + '/project/:id', {
            id: '@id'
        }, {
            update: {
                method: 'PUT'
            },
            get: {
                method: 'GET',
                isArray: false,
                transformResponse: function(data, headers) {
                    // Inicia o transformResponse glogal que retorna a data já
                    // serializado em Json
                    var tr = appConfig.utils.transformResponse( data, headers );
                    var tmpDueDate = tr.due_date.split('-');
                    tr.due_date = new Date( tmpDueDate[0], tmpDueDate[1] - 1, tmpDueDate[2] );
                    tr.client_id = tr.client.id;
                    return tr;
                }
            }
        });
    }]);
