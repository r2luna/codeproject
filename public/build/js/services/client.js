angular.module('app.services')
    .service('Client', ['$resource', 'appConfig', function($resource, appConfig) {
        return $resource(appConfig.baseUrl + '/client/:id', {
            id: '@id'
        }, {
            update: {
                method: 'PUT'
            },
            cs: {
                method: 'GET',
                isArray: true,
                params: {
                    id: 'cs',
                    filter: 'id;name',
                    orderBy: 'name'
                }
            }

            // ==

            // Esta opção serve para transformar a resposta
            // que vem do servidor, quando utilizamos os transformers
            // no Laravel\Fractal por padrão ele envia as informações
            // dentro de um objeto chamado 'data', e o angular se perde
            // com isso, por isso por isso é preciso transformar o resultado.
            // --
            // Mas, eu já tinha modificado o retorno do Laravel, criando uma
            // classe intermediária de Serializer: DataArraySerializer.php
            // que remove o data na transformação dos dados, então
            // não é preciso fazer nada on angular.

            // --
            // ,query: {
            //     method: 'GET',
            //     isArray: true,
            //     transformeResponse: function(data, headers) {
            //         var dataJson = JSON.parse( data );
            //         dataJson = dataJson.data;
            //         return dataJson;
            //     }
            // }

            // ==
        });
    }]);
