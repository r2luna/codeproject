<?php

namespace CodeProject\Entities;

use CodeProject\Entities\Project;
use CodeProject\Entities\ProjectMembers;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function projectsAsOwner()
    {
        return $this->hasMany(Project::class);
    }

    public function projectsAsMember() { 
        return $this->hasMany(Project::class, 'project_members', 'user_id'); 
    }


}
