<?php

namespace CodeProject\Entities;

use CodeProject\Entities\Project;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
    	'name',
    	'responsible',
    	'email',
    	'phone',
    	'address',
    	'obs'
    ];

    public function projects()
    {
    	return $this->hasMany(Project::class);
    }
}