<?php

namespace CodeProject\Services;

use CodeProject\Repositories\ProjectFileRepository;
use CodeProject\Validators\ProjectFileValidator;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Contracts\Filesystem\Factory as Storage;

class ProjectFileService extends AbstractService
{

	/**
	 * @var Filesystem
	 */
	protected $filesystem;

	/**
	 * @var Storage
	 */
	protected $storage;

	public function __construct(ProjectFileRepository $repository, ProjectFileValidator $validator, Filesystem $filesystem, Storage$storage)
	{
		$this->repository = $repository;
		$this->validator = $validator;
		$this->filesystem = $filesystem;
		$this->storage = $storage;
	}

	public function create( array $data )
	{

		// Verifica se o arquivo foi informado
		$file = $data['file'];
		if( is_null( $file ) )
			return [ 'error' => true, 'message' => [ 'description' => [ 'The file field is required.' ] ] ];

		$extension = $file->getClientOriginalExtension();
		$data['extension'] = $extension;

		// Utiliza o método pai para gravar no banco de dados
		$file = parent::create($data)['data'];

		if( !isset( $file['error'] ) ) {
			// Grava no banco de dados
			$this->storage->put( 
				$file['id'].'.'.$data['extension'],
				$this->filesystem->get( $data['file'] ) 
			);
		}

		return $file;
	}

	public function update( array $data, $fileId )
	{
		// Certificamos de que as informações sensíveis
		// do registro do arquivo não sejam alteradas
		unset( $data['project_id'] );
		unset( $data['name'] );
		unset( $data['extension'] );

		return parent::update( $data, $fileId );
	}

	public function delete( $fileId )
	{
		$file = $this->repository->skipPresenter()->find( $fileId );

		if( is_null( $file ) ) return;

		$filename = $file->id.'.'.$file->extension;

		if( $this->storage->exists( $filename ) )	
			$this->storage->delete( $filename );

		return parent::delete( $fileId );
	}

}