<?php

namespace CodeProject\Services;

use CodeProject\Repositories\ProjectNoteRepository;
use CodeProject\Validators\ProjectNoteValidator;

class ProjectNoteService extends AbstractService
{

	public function __construct(ProjectNoteRepository $repository, ProjectNoteValidator $validator)
	{
		$this->repository = $repository;
		$this->validator = $validator;
	}

	public function update( array $data, $noteId )
	{
	

		unset( $data['project_id'] );
		unset( $data['created_at'] );

		return parent::update( $data, $noteId );
	}

}
