<?php

namespace CodeProject\Services;

use CodeProject\Entities\User;
use CodeProject\Repositories\ProjectRepository;
use CodeProject\Validators\ProjectValidator;

class ProjectService extends AbstractService
{
	public function __construct(ProjectRepository $repository, ProjectValidator $validator)
	{
		$this->repository = $repository;
		$this->validator = $validator;
	}

	/**
	 * Adiciona um usuário como membro do projeto
	 * 
	 * @param int
	 * @param int
	 * @return string
	 */
	public function addMember($id, $memberId)
	{
		try {

			// --
			// Verifica se o usuário existe
			$member = User::find( $memberId );
			if( is_null( $member ) )
				throw new \Exception("Usuário não existe.", 1);

			// --
			// Verifica se o usuário já está cadastrado
			if( $this->repository->isMember( $id, $memberId ) )
				throw new \Exception("Usuário já cadastrado como membro do projeto.", 1);

			// --
			// Vincula o usuário ao projeto
			$project = $this->repository->find( $id );
			$project->members()->attach( $member );

			return 'true';

		} catch (\Exception $e) {
			return [
				'error' => true,
				'message' => $e->getMessage()
			];
		}
	}

	/**
	 * Remove o usuário do projeto
	 * 
	 * @param int
	 * @param int
	 */
	public function removeMember($id, $memberId)
	{
		$this->repository->find( $id )->members()->detach( $memberId );
	}


}