<?php

namespace CodeProject\Services;

use CodeProject\Repositories\ProjectTaskRepository;
use CodeProject\Validators\ProjectTaskValidator;

class ProjectTaskService extends AbstractService
{
	public function __construct(ProjectTaskRepository $repository, ProjectTaskValidator $validator)
	{
		$this->repository = $repository;
		$this->validator = $validator;
	}

	public function update( array $data, $taskId )
	{
		unset( $data['project_id'] );

		return pareng::update( $data, $taskId );
	}

}