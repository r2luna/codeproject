<?php

namespace CodeProject\Services;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

abstract class AbstractService implements ServiceInterface {

	protected $repository;
	protected $validator;

	public function create(array $data)
	{
		try {

			$this->validator->with( $data )->passesOrFail( ValidatorInterface::RULE_CREATE );
			return $this->repository->create( $data );

		} catch (ValidatorException $e) {
			return [
				'error' => true,
				'message' => $e->getMessageBag()
			];
		}

	}

	public function update(array $data, $id)
	{
		try {

			$this->validator->with( $data )->passesOrFail( ValidatorInterface::RULE_UPDATE );
		
			return $this->repository->update( $data, $id );

		} catch (ValidatorException $e) {
			return [
				'error' => true,
				'message' => $e->getMessageBag()
			];
		}
	}

	public function delete($id)
	{
		return $this->repository->delete( $id );
	}


}
