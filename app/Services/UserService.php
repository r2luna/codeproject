<?php

namespace CodeProject\Services;

use CodeProject\Repositories\UserRepository;
use CodeProject\Validators\UserValidator;

class UserService extends AbstractService
{
	public function __construct(UserRepository $repository, UserValidator $validator)
	{
		$this->repository = $repository;
		$this->validator = $validator;
	}
}