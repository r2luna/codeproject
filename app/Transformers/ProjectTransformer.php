<?php

namespace CodeProject\Transformers;

use CodeProject\Entities\Project;
use League\Fractal\TransformerAbstract;
use Carbon\Carbon;

class ProjectTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'members',
        'notes',
        'tasks',
        'files',
    ];

    public function transform(Project $project)
    {
        return [
            'project_id' => $project->id,
            'name' => $project->name,
            'owner' => [
                'id' => $project->owner->id,
                'name' => $project->owner->name,
                'email' => $project->owner->email,
            ],
            'client' => [
                'id' => $project->client->id,
                'name' => $project->client->name,
                'email' => $project->client->email,
            ],
            'members' => $project->members,
            'notes' => $project->notes,
            'tasks' => $project->tasks,
            'files' => $project->files,
            'description' => $project->description,
            'progress' => (int) $project->progress,
            'status' => (int) $project->status,
            'due_date' => $project->due_date,
        ];
    }

    // --
    // Métodos de Includes -> transformers das entidades relacionadas
    // --

    public function includeMembers(Project $project)
    {
        return $this->collection($project->members, new ProjectMemberTransformer());
    }

    public function includeNotes(Project $project)
    {
        return $this->collection($project->notes, new ProjectNoteTransformer());
    }

    public function includeTasks(Project $project)
    {
        return $this->collection($project->tasks, new ProjectTaskTransformer());
    }

    public function includeFiles(Project $project)
    {
        return $this->collection($project->files, new ProjectFileTransformer());
    }
}
