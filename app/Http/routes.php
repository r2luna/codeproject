<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here iste where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('app');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['web']], function () {

});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| This route group applies the "api" middleware group to every route
| it contains. And uses the prefix api, every url inside this group
| will starts with api, ex.:
| http://localhost:8000/api/client
|
*/

// --
// Controle de autenticação oAuth
Route::post('api/oauth/access_token', function () {
    return Response::json(Authorizer::issueAccessToken());
});

Route::group([
        'middleware' => ['api'], // Define qual middleware vai ser ativado para esse grupo
        'prefix' => 'api', // Define um prefixo para a url /api/{rota}
        'as' => 'api::', // Nomeia o grupo de rota para gerar url mais fácil no código Ex.: route("api::client")
    ], function () {

    // --
    // User authentication
    Route::get('user/authenticated', 'UserController@authenticated');

    // --
    // Client
    Route::get('client/cs', 'ClientController@cleanGet');
    Route::resource('client', 'ClientController', ['except' => ['create', 'edit']]);

    // --
    // Projects
    Route::group(['prefix' => 'project'], function () {

        // --
        // Project Notes
        Route::group(['prefix' => '{id}/note', 'middleware' => 'CheckProjectPermission:both'], function ($id) {
            Route::get('', 'ProjectNoteController@index');
            Route::post('', 'ProjectNoteController@store');
            Route::get('{noteId}', 'ProjectNoteController@show');
            Route::put('{noteId}', 'ProjectNoteController@update');
            Route::delete('{noteId}', 'ProjectNoteController@destroy');
        });

        // --
        // Project Tasks
        Route::group(['prefix' => '{id}/task', 'middleware' => 'CheckProjectPermission:both'], function () {
            Route::get('', 'ProjectTaskController@index');
            Route::post('', 'ProjectTaskController@store');
            Route::get('{taskId}', 'ProjectTaskController@show');
            Route::put('{taskId}', 'ProjectTaskController@update');
            Route::delete('{taskId}', 'ProjectTaskController@destroy');
        });

        // --
        // Project Member
        Route::group(['prefix' => '{id}/member/{memberId}', 'middleware' => 'CheckProjectPermission:owner'], function () {
            Route::post('', 'ProjectController@addMember');
            Route::delete('', 'ProjectController@removeMember');
            Route::get('', 'ProjectController@isMember');
        });

        // --
        // Project Files
        Route::group(['prefix' => '{id}/file', 'middleware' => 'CheckProjectPermission:both'], function () {
            Route::get('', 'ProjectFileController@index');
            Route::post('', 'ProjectFileController@store');
            Route::get('{fileId}', 'ProjectFileController@show');
            Route::put('{fileId}', 'ProjectFileController@update');
            Route::delete('{fileId}', 'ProjectFileController@destroy');
        });

        Route::get('{id}/members', [
            'middleware' => 'CheckProjectPermission:both',
            'uses' => 'ProjectController@members',
        ]);

    });

    // --
    // Project
    Route::resource('project', 'ProjectController', ['except' => ['create', 'edit']]);

});
