<?php

namespace CodeProject\Http\Middleware;

use Closure;
use CodeProject\Repositories\ProjectRepository;

class CheckProjectPermission
{

    /**
     * @var ProjectRepository
     */
    protected $repository;

    public function __construct( ProjectRepository $repository )
    {
        $this->repository = $repository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        $projectId = $request->id;
        $userId = \Authorizer::getResourceOwnerId();
        $accessForbidden = response([ 'error' => 'invalid_grant' ], 400);
        $hasAccess = false;

        switch ($role) {
            case 'owner':
                $hasAccess = $this->repository->isOwner( $projectId, $userId );
                break;
            case 'member':
                $hasAccess = $this->repository->isMember( $projectId, $userId );
                break;
            case 'both':
                $hasAccess = $this->repository->isRelatedProject( $projectId, $userId );
                break;
        }

        if( !$hasAccess )
            return $accessForbidden;

        return $next($request);
    }
}
