<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Repositories\UserRepository;
use LucaDegasperi\OAuth2Server\Authorizer;

class UserController extends Controller
{
    private $repository;
    private $authorizer;

    public function __construct(UserRepository $repository, Authorizer $authorizer)
    {
        $this->repository = $repository;
        $this->authorizer = $authorizer;
    }

    public function authenticated($value = '')
    {
        $idUser = $this->authorizer->getResourceOwnerId();

        return $this->repository->find($idUser);
    }
}
