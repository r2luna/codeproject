<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Http\Controllers\Controller;
use CodeProject\Http\Requests;
use CodeProject\Repositories\ProjectRepository;
use CodeProject\Services\ProjectService;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    /**
     * @var ProjectRepository
     */
    private $repository;

    /**
     * @var ProjectService
     */
    private $service;

    /**
     * @var int
     */
    private $userId;

    /**
     * @var Array
     */
    private $accessForbidden;

    public function __construct(ProjectRepository $repository, ProjectService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
        $this->userId = \Authorizer::getResourceOwnerId();
        $this->accessForbidden = response([ 'error' => 'You don\'t have access to this project!' ], 401);
    }

    /**
     * Listar todos os projetos
     * @return CodeProject\Project[]
     */
    public function index()
    {
        return $this->repository->findRelatedProjects($this->userId);
    }

    /**
     * Retorna o projeto
     * @param int $id
     * @return CodeProject\Project
     */
    public function show($id)
    {
        $project = $this->repository->getRelatedProject($id, $this->userId);

        if (is_null($project)) {
            return $this->accessForbidden;
        }

        return count( $project ) > 0 ? $project[0] : null;
    }

    /**
     * Salvar um projeto
     * @param Request $request
     * @return CodeProject\Project
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * Atualiza o registro
     * @param Request $request
     * @param int $id
     * @return CodeProject\Project
     */
    public function update(Request $request, $id)
    {
        if ( !( $this->repository->isOwner($id, $this->userId) || $this->repository->isMember($id, $this->userId) ) ) {
            return $this->accessForbidden;
        }

        return $this->service->update($request->all(), $id);
    }

    /**
     * Deleta o registro
     * @param int $id
     */
    public function destroy($id)
    {
        if (!$this->repository->isOwner($id, $this->userId)) {
            return $this->accessForbidden;
        }

        return $this->service->delete($id);
    }

    /**
     * Lista todos os membros de um projecto
     * @param CodeProject\Users[]
     */
    public function members($id)
    {
        return $this->repository->find($id)->members;
    }

    /**
     * Adiciona um novo membro para o projeto
     * @param int $id Id do Projecto
     * @param int $memberId Id do Usuário
     * @return User
     */
    public function addMember($id, $memberId)
    {
        return $this->service->addMember($id, $memberId);
    }

    /**
     * Verifica se o usuário é membro do projeto
     * @param int $id Id do Projecto
     * @param int $memberId Id do Usuário
     * @return boolean
     */
    public function isMember($id, $memberId)
    {
        return json_encode($this->repository->isMember($id, $memberId));
    }

    /**
     * Remove o usuário como membro do projeto
     * @param int $id Id do Projecto
     * @param int $memberId Id do Usuário
     */
    public function removeMember($id, $memberId)
    {
        $this->service->removeMember($id, $memberId);
    }
}
