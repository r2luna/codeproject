<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Http\Controllers\Controller;
use CodeProject\Http\Requests;
use CodeProject\Repositories\ClientRepository;
use CodeProject\Services\ClientService;
use Illuminate\Http\Request;

class ClientController extends Controller
{

	/**
	 * @var ClientRepository
	 */
	private $repository;

	/**
	 * @var ClientService
	 */
	private $service;

	function __construct(ClientRepository $repository, ClientService $service) {
		$this->repository = $repository;
		$this->service = $service;
	}

	/**
	 * Listar todos os clientes
	 * @return CodeProject\Client[]
	 */
	public function index()
	{
		return $this->repository->all();
	}

	public function cleanGet()
	{
		return $this->repository->skipPresenter()->all();
	}

	/**
	 * Retorna o cliente
	 * @param int $id
	 * @return CodeProject\Client
	 */
	public function show($id)
	{
		return $this->repository->find($id);
	}

	/**
	 * Salvar um cliente
	 * @param Request $request
	 * @return CodeProject\Client
	 */
	public function store(Request $request)
	{
		return $this->service->create( $request->all() );
	}

	/**
	 * Atualiza o registro
	 * @param Request $request
	 * @param int $id
	 * @return CodeProject\Client
	 */
	public function update(Request $request, $id)
	{
		return $this->service->update( $request->all(), $id );
	}

	/**
	 * Deleta o registro
	 * @param int $id
	 */
	public function destroy($id)
	{
		return $this->service->delete( $id );
	}

}
