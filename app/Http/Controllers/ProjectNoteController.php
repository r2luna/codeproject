<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Http\Controllers\Controller;
use CodeProject\Http\Requests;
use CodeProject\Repositories\ProjectNoteRepository;
use CodeProject\Services\ProjectNoteService;
use Illuminate\Http\Request;

class ProjectNoteController extends Controller
{

	/**
	 * @var ProjectNoteRepository
	 */
	private $repository;

	/**
	 * @var ProjectNoteService
	 */
	private $service;

	function __construct(ProjectNoteRepository $repository, ProjectNoteService $service) {
		$this->repository = $repository;
		$this->service = $service;
	}

	/**
	 * Listar todos os clientes
	 * @return CodeProject\ProjectNote[]
	 */
	public function index($id)
	{
		return $this->repository->findWhere([ 'project_id' => $id ]);
	}

	/**
	 * Retorna o cliente
	 * @param int $id
	 * @return CodeProject\ProjectNote
	 */
	public function show($id, $noteId)
	{
		$tmp = $this->repository->findWhere([
				'project_id' => $id,
				'id' => $noteId
			]);

		return count( $tmp ) > 0 ? $tmp[0] : null;
	}

	/**
	 * Salvar um cliente
	 * @param Request $request
	 * @return CodeProject\ProjectNote
	 */
	public function store(Request $request)
	{
		return $this->service->create( $request->all() );
	}

	/**
	 * Atualiza o registro
	 * @param Request $request
	 * @param int $id
	 * @return CodeProject\ProjectNote
	 */
	public function update(Request $request, $id, $noteId)
	{
		return $this->service->update( $request->all(), $noteId );
	}

	/**
	 * Deleta o registro
	 * @param int $id
	 */
	public function destroy($id, $noteId)
	{
		return $this->service->delete( $noteId );
	}

}
