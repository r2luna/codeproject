<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Http\Controllers\Controller;
use CodeProject\Http\Requests;
use CodeProject\Repositories\ProjectTaskRepository;
use CodeProject\Services\ProjectTaskService;
use Illuminate\Http\Request;

class ProjectTaskController extends Controller
{

	/**
	 * @var ProjectTaskRepository
	 */ 
	private $repository;
	
	/**
	 * @var ProjectTaskService
	 */
	private $service;

	function __construct(ProjectTaskRepository $repository, ProjectTaskService $service) {
		$this->repository = $repository;
		$this->service = $service;
	}

	/**
	 * Listar todos os registros
	 * @return CodeProject\ProjectTask[]
	 */
	public function index($id)
	{
		return $this->repository->findWhere([ 'project_id' => $id ]);
	}

	/**
	 * Retorna um registro
	 * @param int $id 
	 * @return CodeProject\ProjectTask
	 */
	public function show($id, $taskId)
	{
		return $this->repository->findWhere([
				'project_id' => $id,
				'id' => $taskId
			]);
	}

	/**
	 * Salvar um registro
	 * @param Request $request 
	 * @return CodeProject\ProjectTask
	 */
	public function store(Request $request)
	{
		return $this->service->create( $request->all() );
	}

	/**
	 * Atualiza o registro
	 * @param Request $request 
	 * @param int $id 
	 * @return CodeProject\ProjectTask
	 */
	public function update(Request $request, $id, $taskId)
	{
		return $this->service->update( $request->all(), $taskId );
	}

	/**
	 * Deleta o registro
	 * @param int $id
	 */
	public function destroy($id, $taskId)
	{
		return $this->service->delete( $taskId );
	}

}
