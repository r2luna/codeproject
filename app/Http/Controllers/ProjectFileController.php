<?php

namespace CodeProject\Http\Controllers;

use CodeProject\Http\Controllers\Controller;
use CodeProject\Http\Requests;
use CodeProject\Repositories\ProjectFileRepository;
use CodeProject\Services\ProjectFileService;
use Illuminate\Http\Request;

class ProjectFileController extends Controller
{
	/**
	 * @var ProjectFileRepository
	 */ 
	private $repository;
	
	/**
	 * @var ProjectFileService
	 */
	private $service;

	function __construct(ProjectFileRepository $repository, ProjectFileService $service) {
		$this->repository = $repository;
		$this->service = $service;
	}

    /**
	 * Listar todos os registros
	 * @return CodeProject\ProjectFile[]
	 */
	public function index($id)
	{
		return $this->repository->findWhere([ 'project_id' => $id ]);
	}

	/**
	 * Retorna um registro
	 * @param int $id 
	 * @return CodeProject\ProjectFile
	 */
	public function show($id, $fileId)
	{
		return $this->repository->findWhere([
				'project_id' => $id,
				'id'         => $fileId
			]);
	}

	/**
	 * Salvar um registro
	 * @param Request $request 
	 * @return CodeProject\ProjectFile
	 */
	public function store( $projectId, Request $request)
	{
		$data = $request->all();
		$data['project_id'] = $projectId;
		$data['file'] = $request->file('file');

		// Grava as informações no banco de dados
		return $this->service->create( $data );
	}

	/**
	 * Atualiza o registro
	 * @param Request $request 
	 * @param int $id 
	 * @return CodeProject\ProjectFile
	 */
	public function update(Request $request, $id, $fileId)
	{
		return $this->service->update( $request->all(), $fileId );
	}

	/**
	 * Deleta o registro
	 * @param int $id
	 */
	public function destroy($id, $fileId)
	{
		return $this->service->delete( $fileId );
	}
}
