<?php

namespace CodeProject\Validators;

use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\LaravelValidator;

class ProjectFileValidator extends LaravelValidator
{
	
	protected $rules = [
	 	ValidatorInterface::RULE_CREATE => [
            'project_id'  => 'required|integer',
			'name'        => 'required',
			'description' => 'required',
			'extension'   => 'required',
			'file'  	  => 'required',
        ],
        ValidatorInterface::RULE_UPDATE => [
			  
        ]
		
	];	

}