<?php 

namespace CodeProject\Repositories;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Prettus\Repository\Eloquent\BaseRepository;
use Symfony\Component\Debug\Exception\FatalErrorException;

abstract class CodeProjectBaseRepository extends BaseRepository
{
	
    public function delete( $id )
	{
		try {
            parent::find($id);
            parent::delete( $id );
            return ['success'=>true, 'message'=>'Registro deletado com sucesso!'];
        } catch(FatalErrorException $e) {
            return ['error'=>true, 'message'=>'Registro não encontrado.'];
        } catch (QueryException $e) {
            return ['error'=>true, 'message'=>'Registro não pode ser excluído pois existem objetos relacionados a ele.'];
        } catch (ModelNotFoundException $e) {
            return ['error'=>true, 'message'=>'Registro não encontrado.'];
        } catch (\ErrorException $e) {
            return ['error'=>true, 'message'=>'Registro não encontrado.'];
        } catch (\Exception $e) {
            return ['error'=>true, 'message'=>'Ocorreu algum erro ao excluir o registro.'];
        }
	}
	
    public function find( $id, $columns = array('*'))
    {
        try {
            return parent::find( $id, $columns );
        } catch(FatalErrorException $e) {
            return ['error'=>true, 'message'=>'Registro não encontrado.'];
        } catch (ModelNotFoundException $e) {
            return ['error'=>true, 'message'=>'Registro não encontrado.'];
        } catch (\ErrorException $e) {
            return ['error'=>true, 'message'=>'Registro não encontrado.'];
        } catch (\Exception $e) {
            return ['error'=>true, 'message'=>'Ocorreu algum erro ao pesquisar o registro.'];
        }
    }
}