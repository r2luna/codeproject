<?php

namespace CodeProject\Repositories;

use CodeProject\Entities\Project;
use CodeProject\Presenters\ProjectPresenter;
use CodeProject\Repositories\Criterias;
use CodeProject\Repositories\ProjectRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ProjectRepositoryEloquent
 * @package namespace CodeProject\Repositories;
 */
class ProjectRepositoryEloquent extends CodeProjectBaseRepository implements ProjectRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Project::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Verifica se o usuário é dono do projeto
     * @param int
     * @param @int
     * @return boolean
     */
    public function isOwner($id, $ownerId)
    {
        return count(
                        $this->skipPresenter()
                             ->findWhere([
                                'id' => $id,
                                'owner_id' => $ownerId
                            ])
                    ) > 0;
    }

    /**
     * Verifica se o usuário é membro do projeto
     * @param int
     * @param int
     * @return boolean
     */
    public function isMember($id, $memberId)
    {
        $this->pushCriteria( new Criterias\IsProjectMemberCriteria( $id, $memberId ) );
        return count( $this->all() ) > 0;
    }

    /**
     * Retorna todos os projetos relacionados ao usuário
     * sendo ele dono ou membro
     * @param int $userId : Id do usuário
     * @return Array \CodeProject\Entities\Project
     */
    public function findRelatedProjects( $userId )
    {
        $this->pushCriteria( new Criterias\RelatedProjectsCriteria( $userId ) );
        return $this->all();
    }

    /**
     * Retorna o projeto solicitado caso o usuário tenha acesso a ele
     * @param int $id : Id do projeto
     * @param int $userId : Id do usuário
     * @return \CodeProject\Entities\Project
     */
    public function getRelatedProject( $id, $userId )
    {
        $this->pushCriteria( new Criterias\RelatedProjectCriteria( $id, $userId ) );
        return $this->all();
    }

    /**
     * Verifica se ele está relacionado ao projeto como dono ou como membro
     * @param int $id : Id do projeto
     * @param int $userId : Id do usuário
     * @return \CodeProject\Entities\Project
     */
    public function isRelatedProject( $id, $userId )
    {
        return count( $this->getRelatedProject( $id, $userId ) ) > 0;
    }

    /**
     * Configurar o Presenter que vamos trabalhar nessa classe
     * @return ProjectPresenter
     */
    public function presenter()
    {
        return ProjectPresenter::class;
    }
}
