<?php

namespace CodeProject\Repositories\Criterias;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
* Criteria para seleção de projetos relacionados a um usuário
* seja ele dono ou um membro do projeto
*/
class RelatedProjectsCriteria implements CriteriaInterface
{

	protected $userId;

	function __construct( $userId )
	{
		$this->userId = $userId;
	}

	public function apply( $model, RepositoryInterface $repository )
	{
		$model = $model->where( 'owner_id' , $this->userId )
						->orWhereHas('members', function( $query ) { 
								$query->where( 'user_id' , $this->userId ); 
							});
		return $model;
	}

}