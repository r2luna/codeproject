<?php

namespace CodeProject\Repositories\Criterias;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * Criteria para seleção de um projecto relacionado a um usuário
 * seja ele dono ou um membro do projeto.
 */
class IsProjectMemberCriteria implements CriteriaInterface
{
    protected $id;
    protected $userId;

    public function __construct($id, $userId)
    {
        $this->id = $id;
        $this->userId = $userId;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('id', $this->id)
                        ->where(function ($query) {
                            return $query->whereHas('members', function ($query2) {
                                        $query2->where('user_id', $this->userId);
                                    });
                        });

        return $model;
    }
}
