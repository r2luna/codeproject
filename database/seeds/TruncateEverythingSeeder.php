<?php

use Illuminate\Database\Seeder;

class TruncateEverythingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	
        DB::statement('SET foreign_key_checks=0');
        CodeProject\Entities\Client::truncate();
        CodeProject\Entities\ProjectNote::truncate();
        CodeProject\Entities\ProjectTask::truncate();
        CodeProject\Entities\ProjectMembers::truncate();
        CodeProject\Entities\Project::truncate();
        CodeProject\Entities\User::truncate();
        DB::statement('SET foreign_key_checks=1');

    }
}
