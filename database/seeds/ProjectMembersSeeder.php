<?php

use CodeProject\Entities\ProjectMembers;
use Illuminate\Database\Seeder;

class ProjectMembersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	factory(ProjectMembers::class, 50)->create();
    }
}
