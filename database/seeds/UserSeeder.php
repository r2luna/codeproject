<?php

use CodeProject\Entities\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// --
    	// Usuário de teste que vamos utilizar
    	// para validar o oAuth
    	factory(User::class)->create([
	        'name' => 'Rafael',
	        'email' => 'rafael@lunardelli.me',
	        'password' => bcrypt(123456),
	        'remember_token' => str_random(10),
		]);

        // --
        // Cria o seed do oAuth
        DB::table('oauth_clients')->insert([
             'id'=>'appid1',
             'secret'=>'secret',
             'name'=>'Codeproject',
             'created_at'=>Carbon\Carbon::now(),
             'updated_at'=>Carbon\Carbon::now()
        ]);


    	factory(User::class, 10)->create();
    }
}
